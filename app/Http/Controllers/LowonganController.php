<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Lowongan;
use Laravel\Socialite\Facades\Socialite;
use App\Models\Lamaran;
use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\Storage;

class LowonganController extends Controller
{
    public function lowongan()
    {
        $lowongan = Lowongan::all();
        return view('lowongan', compact('lowongan'));
    }

    public function detail_lowongan($id)
    {
        $lowongan = Lowongan::findOrFail($id);
        return view('detail_lowongan', compact('lowongan'));
    }

    public function registrasi($id_lowongan)
    {
        if (Auth::check()) {

            $userAuth = Auth::id();
            $user = User::where('id', $userAuth)->first();

            return view('registrasi', compact('user', 'id_lowongan'));
        }
        return view('login');
    }

    public function simpanLamaran(Request $request)
    {
        $fileName = $request->file('file_cv')->getClientOriginalName();
        $path = $request->file('file_cv')->storeAs('files',$fileName,'public');

        $lamaran = Lamaran::create([
            'user_id' => $request->user_id,
            'lowongan_id' => $request->lowongan_id,
            'no_telp' => $request->no_telp,
            'pendidikan' => $request->pendidikan,
            'bidang_study' => $request->bidang_studi,
            'daerah' => $request->asal_daerah,
            'instagram' => $request->social_media_ig,
            'facebook' => $request->social_media_fb,
            'git' => $request->git_akun,
            'cv_path' => $fileName,
            'status' => 'Menunggu',
        ]);
        $lamaran->save();

        return redirect('/lowongan')->with('success', 'Employee create successfully.');
    }

    
}
