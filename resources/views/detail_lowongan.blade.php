<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <title>Detail Lowongan</title>
</head>

<body>
    <div class="container mt-4 border shadow rounded p-4">
        <h1>{{$lowongan->judul}}</h1>
        <h5>{{$lowongan->tipe_pekerjaan}}</h5>
        <br>
        <div class="row">
            <div class="col-11">
                <strong>Gaji</strong>
                <h5>Rp. {{number_format($lowongan->gaji,2,',','.')}}</h5>
            </div>
            <div class="col-1">
                <a href="" class="btn btn-success">Apply</a>
            </div>
        </div>
        <br>
        <strong>Skill yang harus dimiliki</strong>
        <div>
            @foreach($lowongan->skill as $skill)
            <button class="btn btn-secondary mt-1" disabled>{{$skill}}</button>
            @endforeach
        </div>
        <br>
        <strong>Deskripsi</strong>
        <p>{{$lowongan->deskripsi}}</p>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>