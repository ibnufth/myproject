<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
</head>
<body>
    <div class="container">
        <h2 class="mb-3 mt-4">Registrasi Kandidat</h2> <hr>
            <form action="{{route('simpan_lamaran')}}" method="POST" class="needs-validator" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="user_id" name="user_id" value="{{$user->id}}">
                <input type="hidden" id="lowongan_id" name="lowongan_id" value="{{$id_lowongan}}">
            <div class="row g-3">
                <div class="col-6">
                    <label for="nama" class="form-label">Nama Lengkap</label>
                    <input type="text" id="nama" name="nama" class="form-control" value="{{$user->name}}">
                </div>
                <div class="col-6">
                    <label for="email" class="form-label">Email</label>
                    <input type="text" id="email" name="email" class="form-control" value="{{$user->email}}">
                </div>
                <div class="col-6">
                    <label for="no_telp" class="form-label">No.Telp/WhatsApp</label>
                    <input type="number" id="no_telp" name="no_telp" class="form-control">
                </div>
                <div class="col-6">
                <label for="pendidikan" class="form-label">Pendidikan</label>
                    <select class="form-select" name="pendidikan" id="pendidikan">
                    <option selected value="">Pendidikan...</option>
                    <option value="SMP/MTS">SMP/MTS</option>
                    <option value="SMA/SMK/MA">SMA/SMK/MA</option>
                    <option value="D3">D3</option>
                    <option value="S1">S1</option>
                    </select>
                </div>
                <div class="col-6">
                    <label for="bidang_studi" class="form-label">Bidang Studi</label>
                    <input type="text" id="bidang_studi" name="bidang_studi" class="form-control">
                </div>
                <div class="col-6">
                    <label for="asal_daerah" class="form-label">Daerah Asal</label>
                    <input type="text" id="asal_daerah" name="asal_daerah" class="form-control">
                </div>
                <div class="col-6">
                    <label for="social_media_ig" class="form-label">Social Media - Instagram</label>
                    <input type="text" id="social_media_ig" name="social_media_ig" class="form-control">
                </div>
                <div class="col-6">
                    <label for="social_media_fb" class="form-label">Social Media - Facebook</label>
                    <input type="text" id="social_media_fb" name="social_media_fb" class="form-control">
                </div>
                <div class="col-6">
                    <label for="git_akun" class="form-label">Akun Gitlab/Github, dsb</label>
                    <input type="text" id="git_akun" name="git_akun" class="form-control">
                </div>
                <div class="col-6">
                    <label for="file_cv" class="form-label">Upload CV *PDF</label>
                    <input type="file" id="file_cv" name="file_cv" class="form-control" accept="application/pdf">
                </div>
                <div class="col d-grid d-md-flex justify-content-end">
                    <button class="btn btn-primary" type="submit">Apply</button>
                </div>
            </div>
            </form>
    </div>

<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>