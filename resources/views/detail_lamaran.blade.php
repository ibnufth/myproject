<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Lamaran</title>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
</head>

<body>
    <div class="container mt-4">
        <h1>{{$lamaran->judul}} - {{$lamaran->name}}</h1>
        <hr>
        <div class="row">
            <div class="col-10">
                <strong>Pendidikan</strong>
                <p>{{$lamaran->pendidikan}}-{{$lamaran->bidang_study}}</p>
            </div>
            <div class="col-1">
                <h2>{{$lamaran->lamaran_status}}</h2>
            </div>
        </div>
        <strong>Asal Daerah</strong>
        <p>{{$lamaran->daerah}}</p>
        <strong>Instagram</strong>
        <p>{{$lamaran->instagram}}</p>
        <strong>Facebook</strong>
        <p>{{$lamaran->facebook}}</p>
        <strong>Gitlab/Github</strong>
        <p>{{$lamaran->git}}</p>
        <strong>CV</strong><br>
        <div class="row">
            <div class="col-10">
                <a href="{{route('downloadcv',$lamaran->cv_path)}}" class="btn btn-primary mt-2">Download CV</a>
            </div>
            <div class="col-2">
                <a href="{{route('tolakLamaran',$lamaran->id)}}" class="btn btn-danger">Tolak</a>
                <a href="{{route('terimaLamaran',$lamaran->id)}}" class="btn btn-success">Terima</a>
            </div>
        </div>

    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>