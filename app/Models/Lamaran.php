<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lamaran extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'lowongan_id',
        'no_telp',
        'pendidikan',
        'bidang_study',
        'daerah',
        'instagram',
        'facebook',
        'git',
        'cv_path',
        'status',
    ];
}
