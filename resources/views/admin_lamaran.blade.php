<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <title>Detail Lamaran</title>
</head>

<body>
    <div class="container mt-4">
        <div class="row">
            <div class="col-10">
                <h1>{{$lamaran[0]->judul}}</h1>
            </div>
            <hr>
        </div>
       @php
       
       @endphp
       @foreach($lamaran as $lamaran)
        <div class="card shadow-sm mt-1">
            <div class="row p-4">
                <div class="col-10">
                    <a href="{{route('detail_lowongan',$lamaran->id)}}" style="text-decoration: none; color:black;">
                        <h3>{{$lamaran->name}}</h3>
                    </a>
                    <p>{{$lamaran->pendidikan}}{{$lamaran->bidang_study}}</p>
                </div>

                <div class="col">
                    <strong class="text-end">{{$lamaran->lamaran_status}}</strong>
                </div>
                <div class="col-10">
                    <h4>{{$lamaran->no_telp}}</h4>
                </div>
                <div class="col">
                    <a class="btn  btn-info btn-sm" href="{{route('admin.lamaranDetail',$lamaran->id)}}">Details</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>