<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lowongan extends Model
{
    use HasFactory;
    protected $fillable = [
        'judul',
        'status',
        'tipe_pekerjaan',
        'tanggal_mulai_lowongan',
        'tanggal_akhir_lowongan',
        'gaji',
        'periode_gaji',
        'skill',
        'deskripsi',
    ];
}
