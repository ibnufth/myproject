<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Lowongan;
use App\Models\Lamaran;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerifikasiLamaran;

class AdminController extends Controller
{
    public function lowongan()
    {
        $lowonganresult = Lowongan::all();
        $lowonganarray = $lowonganresult->toArray();
        $lowongan = array();
        foreach ($lowonganarray as $lowongandata) {
            $data = Lamaran::where('lowongan_id', $lowongandata['id'])->count();
            $array_data = array('jumlah_kandidat' => $data);
            array_push($lowongan, array_merge($array_data, $lowongandata));
        }

        return view('admin_lowongan', compact('lowongan'));
    }
    public function buat_lowongan()
    {
        return view('admin_buat_lowongan');
    }

    public function simpan(Request $request)
    {
        $lowongan = $request->validate([
            'judul' => 'required',
            'status' => 'required',
            'tipe_pekerjaan' => 'required',
            'tanggal_mulai_lowongan' => 'required',
            'tanggal_akhir_lowongan' => 'required',
            'gaji' => 'required',
            'periode_gaji' => 'required',
            'skill' => 'required',
            'deskripsi' => 'required',
        ]);

        Lowongan::create($lowongan);
        return redirect('admin/lowongan')->with('success', 'Employee create successfully.');
    }

    public function edit($id)
    {
        $lowongan = Lowongan::findOrFail($id);
        return view('admin_edit_lowongan', compact('lowongan'));
    }

    public function update(Request $request, $id)
    {
        $lowonganData = $request->validate([
            'judul' => 'required',
            'status' => 'required',
            'tipe_pekerjaan' => 'required',
            'tanggal_mulai_lowongan' => 'required',
            'tanggal_akhir_lowongan' => 'required',
            'gaji' => 'required',
            'periode_gaji' => 'required',
            'skill' => 'required',
            'deskripsi' => 'required',
        ]);
        $lowongan = Lowongan::findOrFail($id);
        $lowongan->judul = $request->judul;
        $lowongan->status = $request->status;
        $lowongan->tipe_pekerjaan = $request->tipe_pekerjaan;
        $lowongan->tanggal_mulai_lowongan = $request->tanggal_mulai_lowongan;
        $lowongan->tanggal_akhir_lowongan = $request->tanggal_akhir_lowongan;
        $lowongan->gaji = $request->gaji;
        $lowongan->periode_gaji = $request->periode_gaji;
        $lowongan->skill = $request->skill;
        $lowongan->deskripsi = $request->deskripsi;
        $lowongan->save();

        return redirect('admin/lowongan');
    }

    public function delete(Lowongan $lowongan)
    {
        $lowongan->delete();

        return redirect('admin/lowongan');
    }

    public function lamaran($id)
    {
        $lamaran = DB::table('lamarans')->join('users', 'users.id', '=', 'lamarans.user_id')
            ->join('lowongans', 'lowongans.id', '=', 'lamarans.lowongan_id')
            ->where('lowongan_id', $id)->get(['*', 'lamarans.status as lamaran_status']);

        return view('admin_lamaran', compact('lamaran'));
    }

    public function lamaranDetail($id)
    {
        $lamaran = DB::table('lamarans')->join('users', 'users.id', '=', 'lamarans.user_id')
            ->join('lowongans', 'lowongans.id', '=', 'lamarans.lowongan_id')
            ->where('lamarans.id', $id)->get(['*', 'lamarans.status as lamaran_status', 'users.id as user_id', 'lowongans.id as lowongan_id'])->first();

        return view('detail_lamaran', compact('lamaran'));
    }

    public function downloadcv($file_path)
    {
        $header = ['Content-Type: application/pdf'];
        $file = storage_path('app/public/files/' . $file_path);
        return response()->download($file, 'CV.pdf', $header);
    }
    public function terimaLamaran($id)
    {
        $lamaran = Lamaran::findOrFail($id);
        $lamaran->status = 'Diterima';
        $lamaran->save();
        $user = DB::table('lamarans')->join('users', 'users.id', '=', 'lamarans.user_id')
            ->where('lamarans.id', $id)->get(['name','email'])->first();
        $detail = [
            'name' => $user->name,
            'tanggal_interview' => date("Y-m-d", strtotime("+1 day")),
            'waktu_interview' => '09:00'
        ];
        // fyxljulhzcwlsavw
        // postmaster@sandbox7c9b19344f16428981b9d82de1e825d8.mailgun.org
        // 27ab3c82916919115ad48d3b57973b6a-443ec20e-ae0c6653
        Mail::to($user->email)->send(new EmailVerifikasiLamaran($detail));
        return redirect()->route('admin.lamaranDetail',[$id]);
    }
    public function tolakLamaran($id)
    {
        $lamaran = Lamaran::findOrFail($id);
        $lamaran->status = 'Ditolak';
        $lamaran->save();
        return redirect()->route('admin.lamaranDetail',[$id]);
    }
}
