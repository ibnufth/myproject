<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
</head>

<body>
    <div class="position-absolute top-50 start-50 translate-middle">
        <a href="{{url('auth/google')}}"><button class="btn btn-primary">Lanjutkan Dengan Google</button></a>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>