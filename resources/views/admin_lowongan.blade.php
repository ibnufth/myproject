<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lowongan</title>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
</head>

<body>

    <div class="container mt-4">
        <div class="row">
            <div class="col-10">
                <h1>Lowongan Pekerjaan DLK</h1>
            </div>
            <div class="col mt-3">
                <a href="{{ route('admin.buat_lowongan')}}" class="btn  btn-success btn-sm">Buat Baru</a>
            </div>
        </div>
        <hr>
        @foreach($lowongan as $lowongan)
        <div class="card shadow-sm mt-1">
            <div class="row p-4">
                <div class="col-10">
                    <a href="{{route('detail_lowongan',$lowongan['id'])}}" style="text-decoration: none; color:black;">
                        <h3>{{$lowongan['judul']}}</h3>
                    </a>
                    <p>{{$lowongan['tanggal_mulai_lowongan']}} hingga {{$lowongan['tanggal_akhir_lowongan']}}</p>
                </div>

                <div class="col">
                    @if($lowongan['tanggal_akhir_lowongan'] >= date("Y-m-d"))
                    <strong class="text-end">OPEN</strong>
                    @else
                    <strong class="text-end">Close</strong>
                    @endif
                </div>
                <div class="col-10">
                    <a href="{{route('admin.lamaran',$lowongan['id'])}}" class="btn btn-secondary" style="text-decoration: none; color:black;">
                        <h4>{{$lowongan['jumlah_kandidat']}} Kandidat</h4>
                    </a>
                </div>
                <div class="col">
                    <form action="{{route('admin.delete',$lowongan['id'])}}" method="post">
                        <a class="btn  btn-info btn-sm" href="{{route('admin.edit',$lowongan['id'])}}">Edit</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn  btn-danger btn-sm">Hapus</button>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>