<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LowonganController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/lowongan', [LowonganController::class, 'lowongan']);
Route::get('auth/google', [LoginController::class, 'googleRedirect']);
Route::get('auth/google/callback', [LoginController::class, 'googleCallback']);
Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::get('/{lowongan}/detail_lowongan', [LowonganController::class, 'detail_lowongan'])->name('detail_lowongan');
Route::get('/{lowongan}/registrasi', [LowonganController::class, 'registrasi'])->name('registrasi');
Route::post('/simpan_lamaran', [LowonganController::class, 'simpanLamaran'])->name('simpan_lamaran');


Route::prefix('admin')->group(function () {
    Route::get('/lowongan', [AdminController::class, 'lowongan'])->name('admin.lowongan');
    Route::get('/buat_lowongan', [AdminController::class, 'buat_lowongan'])->name('admin.buat_lowongan');
    Route::post('/simpan', [AdminController::class, 'simpan'])->name('admin.simpan');
    Route::get('/{lowongan}/edit', [AdminController::class, 'edit'])->name('admin.edit');
    Route::patch('/{lowongan}/update', [AdminController::class, 'update'])->name('admin.update');
    Route::delete('/{lowongan}/delete', [AdminController::class, 'delete'])->name('admin.delete');
    Route::get('/{lamaran}/lamaran', [AdminController::class, 'lamaran'])->name('admin.lamaran');
    Route::get('/{lamaran}/lamarandetail', [AdminController::class, 'lamaranDetail'])->name('admin.lamaranDetail');
    Route::get('/downloadcv/{file_path}', [AdminController::class, 'downloadcv'])->name('downloadcv');
    Route::get('/{data}/terima_lamaran', [AdminController::class, 'terimalamaran'])->name('terimaLamaran');
    Route::get('/{data}/tolak_lamaran', [AdminController::class, 'tolaklamaran'])->name('tolakLamaran');
});
