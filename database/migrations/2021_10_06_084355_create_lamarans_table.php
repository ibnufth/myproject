<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLamaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lamarans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('lowongan_id');
            $table->bigInteger('no_telp');
            $table->string('pendidikan');
            $table->string('bidang_study');
            $table->string('daerah');
            $table->string('instagram');
            $table->string('facebook');
            $table->string('git');
            $table->string('cv_path');
            $table->string('status');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('lowongan_id')->references('id')->on('lowongans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lamarans');
    }
}
