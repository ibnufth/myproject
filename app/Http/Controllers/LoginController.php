<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;

class LoginController extends Controller
{
    public function login()
    {
        return view('login');
    }
    public function googleRedirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback()
    {
        try{
            $user = Socialite::driver('google')->user();
            $findUser = User::where('google_id',$user->id)->first();

            if($findUser){
                Auth::login($findUser);
                return redirect('/lowongan');
            } else {
                $newUser = User::create([
                    'name'=>$user->name,
                    'email'=>$user->email,
                    'google_id'=>$user->id,
                    'password'=> encrypt('halobos'),
                    'position'=>'Kandidat'
                ]);
                $newUser->save();
                Auth::login($newUser);
                return redirect('/lowongan');
            }
        } catch (Exception $e){
            dd($e->getMessage());
            return redirect('/lowongan');
        }
    }
}
