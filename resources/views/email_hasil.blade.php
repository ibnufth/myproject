<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hasil Lamaran DLK</title>
</head>

<body>
    <p>Assalamualaikum wr. wb.</p>
    <strong>Kepada {{$details['name']}}</strong>
    <p>Setelah melihat CV yang telah anda kirimkan ke kami dan melihat sesuai dengan lowongan yang perusahaan kami butuhkan,
    dengan ini kiranya dapat menghadiri undangan interview pada tanggal <strong>{{$details['tanggal_interview']}} pukul {{$details['waktu_interview']}}</strong>.
    Apabila berhalangan anda dapat membalas email ini untuk konfirmasi tanggal.
    </p>
    <p>Demikian yang dapat kami sampaikan. Apabila ada yang ditanyakan silahkan  menghubungi kami.</p>
    <p>Terimakasih atas perhatian dan kerjasamanya</p>

    <p>Assalamualaikum wr. wb.</p><br>
    <small>Hormat Kami</small><br>
    <strong>DLK Creative Lab</strong>

</body>
</body>

</html>