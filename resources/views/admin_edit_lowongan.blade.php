<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Lowongan</title>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>

<body>
    <div class="container">
        <h2 class="mb-3 mt-4">Buat Lowongan</h2>
        <hr>
        <form action="{{route('admin.update', $lowongan->id)}}" method="POST" class="needs-validator">
            @csrf
            @method('PATCH')
            <div class="row g-3">
                <div class="col-6">
                    <label for="judul" class="form-label">Judul</label>
                    <input type="text" id="judul" name="judul" class="form-control" value="{{$lowongan->judul}}">
                </div>
                <div class="col-6">
                    <label for="status" class="form-label">Status</label>
                    <select class="form-select" name="status" id="status">
                        <option selected value="{{$lowongan->status}}">{{$lowongan->status}}</option>
                        <option value="draft">Draft</option>
                        <option value="publish">Publish</option>
                        <option value="close">Close</option>
                    </select>
                </div>
                <div class="col-6">
                    <label for="tipe_pekerjaan" class="form-label">Type Lowongan</label>
                    <select class="form-select" name="tipe_pekerjaan" id="tipe_pekerjaan">
                        <option selected value="{{$lowongan->tipe_pekerjaan}}">{{$lowongan->tipe_pekerjaan}}</option>
                        <option value="Fulltime-Onsite">Fulltime-Onsite</option>
                        <option value="Fulltime-Remote">Fulltime-Remote</option>
                        <option value="Fulltime-Onsite/Remote">Fulltime-Onsite/Remote</option>
                        <option value="Part-time">Part-time</option>
                        <option value="Project-based">Project-based</option>
                    </select>
                </div>
                <div class="col-3">
                    <label for="tanggal_mulai_lowongan" class="form-label">Tanggal Mulai Lowongan</label>
                    <input type="date" class="form-control" id="tanggal_mulai_lowongan" name="tanggal_mulai_lowongan" value="{{$lowongan->tanggal_mulai_lowongan}}">
                </div>
                <div class="col-3">
                    <label for="tanggal_akhir_lowongan" class="form-label">Tanggal Akhir Lowongan</label>
                    <input type="date" class="form-control" id="tanggal_akhir_lowongan" name="tanggal_akhir_lowongan" value="{{$lowongan->tanggal_akhir_lowongan}}">
                </div>
                <div class="col-3">
                    <label for="gaji" class="form-label">Gaji</label>
                    <input type="text" class="form-control" id="gaji" name="gaji" value="{{$lowongan->gaji}}">
                </div>
                <div class="col-3">
                    <label for="periode_gaji" class="form-label">Per</label>
                    <input type="text" class="form-control" id="periode_gaji" name="periode_gaji" placeholder="Bulan" value="{{$lowongan->periode_gaji}}">
                </div>
                <div class="col-6">
                    <label for="skill" class="form-label">Skill yang harus dimiliki</label>
                    <select class="select2-multiple form-control " name="skill" id="skill" multiple="multiple">
                        @foreach($lowongan->skill as $skill)
                        <option selected value="{{$skill}}">{{$skill}}</option>
                        @endforeach
                        <option value="PHP">PHP</option>
                        <option value="Python">Python</option>
                        <option value="Laravel">Laravel</option>
                        <option value="Javascript">Javascript</option>
                        <option value="Flutter">Flutter</option>
                        <option value="SQL">SQL</option>
                    </select>
                </div>
                <div class="col-12">
                    <label for="deskripsi" class="form-label">Deskripsi</label>
                    <textarea class="form-control" name="deskripsi" id="deskripsi" cols="30" rows="5">{{$lowongan->deskripsi}}</textarea>
                </div>
            </div>
            <div class="d-grid gap-2 d-md-flex justify-content-end mt-2">
                <button type="submit" class="btn btn-primary me-md-2">Update</button>
            </div>
        </form>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2-multiple').select2({
                tags: true
            });
        });
    </script>
</body>

</html>