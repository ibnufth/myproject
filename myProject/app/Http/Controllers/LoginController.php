<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class LoginController extends Controller
{
    //
    use AuthenticatesUsers;

    protected $redirecTo = RouteServiceProvider::HOME;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }



    public function google()
    {
        return Socialite::driver('google')->redirect();
    }

    public function google_callback()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();
            $isUser = User::where('google_id', $user->id)->first();

            if ($isUser) {
                Auth::login($isUser);
                return redirect('/home');
            } else {
                $createUser = new User;
                $createUser->name = $user->getName();

                if ($user->getEmail() != null) {
                    $createUser->email = $user->getEmail();
                    $createUser->email_verived_at = \Carbon\Carbon::now();
                }
                $createUser->google_id = $user->getId();

                    $rand = rand(111111, 999999);
                $createUser->password = Hash::make($user->getName() . $rand);
                $createUser->position = 'Kandidat';
                $createUser->save();

                Auth::login($createUser);
                return redirect('/home');
            }
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
