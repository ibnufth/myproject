<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lowongan extends Model
{
    use HasFactory;
    protected $fillable = [
        'judul',
        'status',
        'tipe_pekerjaan',
        'tanggal_mulai_lowongan',
        'tanggal_akhir_lowongan',
        'gaji',
        'periode_gaji',
        'skill',
        'deskripsi',
    ];

    public function setSkillAttribute($value)
    {
        $this->attributes['skill'] = json_encode($value);
    }

    public function getSkillAttribute($value)
    {
        if (is_array($value)) {
            return $this->attributes['skill'];
        } else {
            $res = str_replace(array('[',']','"'),"",$value);
            return $this->attributes['skill'] =  explode(',',$res );
        }
    }
}
