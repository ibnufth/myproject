<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lowongan</title>
    <link rel="stylesheet" href={{ asset('css/app.css') }}>
</head>

<body>

    <div class="container-sm mt-4">
        <h1>Lowongan Pekerjaan DLK</h1>
        <hr>
        @foreach($lowongan as $lowongan)
        <div class="card">
            <div class="row g-0 p-4">
                <div class="col-md-10">
                    <a href="{{route('detail_lowongan',$lowongan->id)}}" style="text-decoration: none; color:black;">
                        <h3>{{$lowongan->judul}}</h3>
                    </a>
                </div>
                
                <div class="col-md-2 d-grid d-md-flex justify-content-end">
                    @if($lowongan->tanggal_akhir_lowongan >= date("Y-m-d"))
                    <strong class="text-end">OPEN</strong>
                    @else
                    <strong class="text-end">Close</strong>
                    @endif
                </div>
                <div class="col-md-10">
                <p>{{$lowongan->tanggal_mulai_lowongan}} hingga {{$lowongan->tanggal_akhir_lowongan}}</p>
                </div>
                <div class="col d-grid d-md-flex justify-content-end">
                    <a href="{{route('registrasi',$lowongan->id)}}"><button class="btn  btn-success btn-sm">Apply</button></a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>